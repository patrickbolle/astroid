Tasks = new Mongo.Collection("tasks");

var Schemas = {};

Schemas.Task = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    description: {
        type: String,
        label: "Description",
        optional: true,
        max: 1000
    },
    category: {
        type: String,
        label: "Category of task"
    },
    priority: {
        type: Number,
        label: "Priority of task",
        min: 1,
        max: 5
    },
    checked: {
        type: Boolean,
        label: "Done or not?",
        optional: true
    },
    createdBy: {
      type: String,
      autoValue: function() {
      return Meteor.user().username
      }
    },
    targetDate: {
        type: Date,
        label: "Projected date to finish",
        autoform: {
          type:"pickadate"
        }
    }
});

Tasks.attachSchema(Schemas.Task);

Tasks.allow({
  insert: function(userId, doc) {
    // only allow posting if you are logged in
    return !! userId;
  },
  update: function(userId, doc) {
    // only allow updating if you are logged in
    return !! userId;
  },
  remove: function(userID, doc) {
    //only allow deleting if you are owner
    return true;
  }
});
