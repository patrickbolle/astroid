//Route for homepage
Router.route('/', function () {
  this.render('homepage'
  );
});

//Route for dashboard

//Route for messaging system
Router.route('/chat', function () {
  this.render('chat', {
    data:  { messages: Messages.find({}) },
    waitOn: function() {
      return Meteor.subscribe('Messages');
    }
  });
});
//Route for to do list
Router.route('/todolist', function () {
  this.render('todolist', {
    data:  { tasks: Tasks.find({}) },
    waitOn: function() {
      return Meteor.subscribe('tasks');
    }
  });
});
