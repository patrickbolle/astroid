Meteor.publish('select-from-tasks', function () {
  return Tasks.find({})
})

Meteor.publish("Messages", function() {
  return Messages.find();
});

Messages.allow({
  'insert': function(userId, doc) {
    return true;
  },
  'remove': function(userId, doc) {
    return false;
  }
});
