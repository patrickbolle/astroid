Template.messages.messages = function() {
  return Messages.find({

  }, {
    sort: {
      timestamp: 1
    },
    limit: 20
  });
};

Template.input.events({
  'click #send': function() {
    var message = $('#newMessage').val();
    Meteor.saveMessage({
      message: message,
      username: Meteor.user().username
    });
  }
});

Meteor.autorun(function() {
  Meteor.subscribe("Messages");
});

Meteor.saveMessage = function(content) {
  var username = content.username;
  var message = content.message;
  if (!username || !message) {
    return;
  }
  Messages.insert({
    username: username,
    message: message,
    timestamp: Date.now()
  }, function(err, id) {
    if (err) {
      alert('Something defnitely went wrong!');
    }
    if (id) {
      $('#newMessage').val('');
      $('#username').val('');
    }
  });
};
