// This code only runs on the client
Meteor.subscribe("select-from-tasks");

Template.todolist.helpers({
    tasks: function() {
      if (Session.get("hideCompleted")) {
         // If hide completed is checked, filter tasks
         return Tasks.find({checked: {$ne: true}}, {sort: {priority: 1}});
       } else {
         // Otherwise, return all of the tasks
         return Tasks.find({}, {sort: {priority: 1}});
       }
     },
     hideCompleted: function () {
       return Session.get("hideCompleted");
     },
     incompleteCount: function () {
       return Tasks.find({checked: {$ne: true}}).count();
     },
     completeCount: function () {
       return Tasks.find({checked: {$ne: false}}).count();
     },
     createChart: function () {
     // Gather data:
     var allTasks = Tasks.find().count(),
           incompleteTask = Tasks.find({checked: {$ne: true}}).count(),
           tasksData = [{
               y: incompleteTask,
               name: "Incomplete Tasks"
            }, {
                y: allTasks - incompleteTask,
                name: "Complete Tasks"
            }];
     // Use Meteor.defer() to craete chart after DOM is ready:
     Meteor.defer(function() {
       // Create standard Highcharts chart with options:
       Highcharts.setOptions({
          colors: ['#EF9A9A', '#69F0AE']
      });
       Highcharts.chart('chart', {
         title: {
          text: ' '
        },
        series: [{
         type: 'pie',
         data: tasksData
        }]
       });
     });
    }
});

Template.todolist.events({
  "change .hide-completed input": function (event) {
    Session.set("hideCompleted", event.target.checked);
  }
});

Template.task.events({
 "click .toggledone": function () {
    // Set the checked property to the opposite of its current value
    Tasks.update(this._id, {
      $set: {checked: ! this.checked}
    });
  },
  "click .delete": function () {
    Tasks.remove(this._id);
  }
});

Accounts.ui.config({
  passwordSignupFields: "USERNAME_ONLY"
});
